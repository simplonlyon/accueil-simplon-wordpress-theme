<?php

/**
 * Template Name: Accueil
 */

get_header();
 ?>

<main id="index">
        <section id="gris">
            <p><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/illustration.png"/>Simplon : Fabrique de développeur.euse.s sociale et solidaire</p>
        </section>
        <h2>Recrutez un.e développeur.euse </br><span>motivé.e</span></h2>
        <!-- Le bouton voir Portfolio envoie désormais pas défaut sur la page de la promo 2-->
        <a id="catch" href="<?php the_permalink(get_option('active_promo')) ?>">Voir les portfolios !</a>
        <p>Les apprenant.e.s de Simplon sont sélectionné.e.s avec soin sur le critère de leur motivation et de leurs capacités à apprendre et travailler en équipe. Leurs implication et compétences sont garanties par la maison !</p>
        <h3>Pour plus d'information sur les programmes et contact :</h3>
        <section id="boutons">
            <a href="https://auvergnerhonealpes.simplon.co/">Consulter le site Simplon Auverne Rhône Alpes</a>
            
        </section>
    </main>


<?php

get_footer();