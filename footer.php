<footer class="sociaux">
	<a href="https://gitlab.com/simplonlyon" aria-label="Lien vers le compte Gitlab de Simplon Lyon" title="Lien vers le compte Gitlab de Simplon Lyon"><i class="fa fa-gitlab fa-3x" aria-hidden="true"></i></a>
        <a href="https://github.com/simplon-lyon" aria-label="Lien vers le compte github de Simplon Lyon" title="Lien vers le compte github de Simplon Lyon"><i class="fa fa-github fa-3x" aria-hidden="true"></i></a>
        <a href="https://twitter.com/SimplonLyon" aria-label="Lien vers la page Twitter de Simplon Lyon" title="Lien vers la page Twitter de Simplon Lyon"><i class="fa fa-twitter fa-3x" aria-hidden="true"></i></a>
        <a href="https://www.linkedin.com/showcase/simplon-auvergne-rh%C3%B4ne-alpes/?viewAsMember=true" aria-label="Lien vers le compte LinkedIn company de Simplon.co" title="Lien vers le compte LinkedIn company de Simplon.co"><i class="fa fa-linkedin fa-3x" aria-hidden="true"></i></a>
        <a href="https://www.facebook.com/SimplonLyon/?ref=br_rs" aria-label="Lien vers la page facebook de Simplon Lyon" title="Lien vers la page facebook de Simplon Lyon"><i class="fa fa-facebook fa-3x" aria-hidden="true"></i></a>
    </footer>

    <?php wp_footer(); ?>
</body>
</html>
