<?php



/**
 * WooCommerce class-wc-api-products.php
 * See https://github.com/justinshreve/woocommerce/blob/master/includes/api/class-wc-api-products.php
 * Upload image from URL
 *
 * @since 2.2
 * @param string $image_url
 * @return int|WP_Error attachment id
 */
/*
function upload_product_image($image_url) {
    $file_name = basename(current(explode('?', $image_url)));
    $wp_filetype = wp_check_filetype($file_name, null);
    $parsed_url = @parse_url($image_url);

    // Check parsed URL
    if(!$parsed_url || !is_array($parsed_url)) {
        throw new Exception('bof');
    }

    // Ensure url is valid
    $image_url = str_replace(' ', '%20', $image_url);

    // Get the file
    $response = wp_safe_remote_get($image_url, array(
        'timeout' => 10
    ));

    if(is_wp_error($response) || 200 !== wp_remote_retrieve_response_code($response)) {
	throw new Exception('bif');
        //throw new WC_API_Exception('woocommerce_api_invalid_remote_product_image', sprintf(__('Error getting remote image %s', 'woocommerce'), $image_url), 400);
    }

    // Ensure we have a file name and type
    if(!$wp_filetype['type']) {
        $headers = wp_remote_retrieve_headers($response);
        if(isset($headers['content-disposition']) && strstr($headers['content-disposition'], 'filename=')) {
            $disposition = end(explode('filename=', $headers['content-disposition']));
            $disposition = sanitize_file_name($disposition);
            $file_name = $disposition;
        }
        elseif(isset($headers['content-type']) && strstr($headers['content-type'], 'image/')) {
            $file_name = 'image.' . str_replace('image/', '', $headers['content-type']);
        }
        unset($headers);
    }

    // Upload the file
    $upload = wp_upload_bits($file_name, '', wp_remote_retrieve_body($response));

    if($upload['error']) {
//        throw new WC_API_Exception('woocommerce_api_product_image_upload_error', $upload['error'], 400);
	throw new Exception('baf');
    }

    // Get filesize
    $filesize = filesize($upload['file']);

    if(0 == $filesize) {
        @unlink($upload['file']);
        unset($upload);
//        throw new WC_API_Exception('woocommerce_api_product_image_upload_file_error', __('Zero size file downloaded', 'woocommerce'), 400);
throw new Exception('bouf');
    }

    unset($response);

    return $upload;
}
*/
/*
function user_set_avatar_url($avatar_url, $user_id) {
        global $wpdb;
        $file = upload_product_image($avatar_url);
        $wp_filetype = wp_check_filetype($file['file']);
        $attachment = array(
            'guid' => $file['url'],
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($file['file'])),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment($attachment, $file['file']);
        $attach_data = wp_generate_attachment_metadata($attach_id, $file['file']);
        wp_update_attachment_metadata($attach_id, $attach_data);
        update_user_meta($user_id, $wpdb->get_blog_prefix() . 'user_avatar', $attach_id);
    }
*/
add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

function my_theme_enqueue_styles()
{
    wp_enqueue_style('child-style', get_stylesheet_uri());
    wp_enqueue_script('child-script', get_stylesheet_directory_uri() .'/assets/menuMobile.js');
}



add_action('user_register', 'create_portfolio');

function create_portfolio($userId)
{

    $user = get_userdata($userId);

    if (array_search('apprenant', $user->roles) != -1 && empty($user->user_url) ) {

        $post_details = array(
            'post_title'    => 'Portfolio de '.$user->user_login,
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => $userId,
            'post_type' => 'page'
        );
        $id = wp_insert_post($post_details);
        
        $user->user_url = get_permalink($id);
        wp_update_user($user);
    }
}



function updateUserAcfFields($id, $post, $acfFieldsId)
{


    global $wpdb;
    $query = $wpdb->prepare("INSERT INTO {$wpdb->prefix}usermeta (user_id,meta_key,meta_value) VALUES ", []);
    $queryValues = [];
    foreach ($post as $key => $value) {

        $queryValues[] = $wpdb->prepare("(%d,%s,%s),(%d,%s,%s)", $id, $key, $value, $id, '_' . $key, $acfFieldsId[$key]);
    }
    $query .= implode(',', $queryValues);
    return $wpdb->query($query);
}


add_action('admin_menu', 'simplon_menu');

function simplon_menu()
{

    add_submenu_page('users.php', 'Simplon', 'Ajouter des Apprenant·e·s', 'manage_options', 'simplon_add_menu', 'add_apprenants');
}


function add_apprenants()
{
    $promos = new WP_Query([
        'post_type' => 'promo',
        'posts_per_page' => -1    
    ]);
    

?>
<div class="wrap">

<!--<form method="post" action="admin-post.php">
<input type="hidden" name="action" value="migration_simplon"/>

<button>Migration</button>

</form>-->
    <h1>Importer des apprenant·e·s</h1>
    <form method="post" action="admin-post.php" enctype="multipart/form-data">
        <input type="hidden" name="action" value="import_apprenants_csv" />
        <p>
            <label for="promo">Promo : </label>
            <select name="promo" id="promo">
            <?php while ($promos->have_posts()) : $promos->the_post(); ?>
                <option value="<?php the_ID() ?>"><?php the_title() ?></option>
            <?php endwhile;
            wp_reset_query(); ?>
            </select>
        </p>
        <p>
            <label for="csv">Fichier csv : </label>
            <input type="file" id="csv" name="csv" accept=".csv" required>
        </p>
        <p>Le csv doit obligatoirement avoir les colonnes suivantes : Nom, Prénom, Mail. <br>Colonnes optionnelles : Entreprise, Type de Contrat</p>
        <button>Importer</button>
    </form>
    </div>
<?php
}
/*
add_action('admin_post_migration_simplon', 'migration_simplon');

function migration_simplon() {
set_time_limit(300);
	$promos = scandir('/var/www/simplonlyon.fr');
//	$promos = array_diff($promos, ['promo12','promo2', 'promo3', 'promo5', 'promo8']);
	foreach($promos as $clef => $promo) {

		if(startsWith($promo, 'promo')) {
			echo $promo . '<br/>';
			$idPromo = wp_insert_post([
				'post_type' => 'promo',
				'post_title' => $promo
			]);
			if(is_wp_error($idPromo)) {
				echo '<p>erreur sur la promo ' .$promo.'</p>';
				continue;
			}
			$apprenants = scandir('/var/www/simplonlyon.fr/'.$promo);
//			if($promo == 'promo9') {
//				$apprenants = array_diff($apprenants, ['jcarneiro']);
//			}
			foreach($apprenants as $clef => $apprenant) {
				if($apprenant != '.' && $apprenant != '..') {
					echo "<p>début $apprenant</p>";
					$jsonPath = '/var/www/simplonlyon.fr/'.$promo.'/'.$apprenant.'/conf.json';
					if(file_exists($jsonPath)) {
						$json = json_decode(file_get_contents($jsonPath), true);
if(is_null($json)) {
	echo "<p>$apprenant non persisté</p>";
	continue;
}
    $id = wp_insert_user([
        'user_email' => $apprenant.'@fake.com',
        'first_name' => $json['prenom'],
        'last_name' => $json['nom'],
        'user_login' => $apprenant,
  	'user_pass' => 'simplonstudent',
        'role' => 'apprenant',
	'user_url' => 'https://www.simplonlyon.fr/'.$promo.'/'.$apprenant
    ]);


    if(is_wp_error($id)) {
        echo '<p>Erreur pour '.$json['Prénom'] . ' ' . $json['Nom'] . ' : <br>';
        echo $id->get_error_message();
        echo '</p>';
     
    } else {

	if(!empty($json['avatar']) && preg_match("/svg$/" , $json['avatar']) == 0 && file_exists('/var/www/simplonlyon.fr/'.$promo.'/'.$apprenant.'/'.$json['avatar'])) {
		try {
		user_set_avatar_url('https://www.simplonlyon.fr/'.$promo.'/'.$apprenant.'/'.$json['avatar'], $id);
		} catch( Exception $e) {
		echo "<p>Erreur pour l'avatar de $apprenant</p>";
		}

	}

     if(!update_field('promo', $idPromo, 'user_'.$id)) {
        echo '<p>Erreur pour '.$data['Prénom'] . ' ' . $data['Nom'] . ' : Promo non assignée.</p>';
     }

     if(!empty($json['entreprise'])) {
        update_field('entreprise', $json['entreprise'], 'user_'.$id);
     }
    
     if(!empty($json['contrat'])) {
        update_field('type_de_contrat', $json['contrat'], 'user_'.$id);
     }


  }
	

	

					}
				}
				
			}
			echo '<br/>';

		}
		
	}

}
*/
function startsWith( $haystack, $needle ) {
     $length = strlen( $needle );
     return substr( $haystack, 0, $length ) === $needle;
}

add_action('admin_post_import_apprenants_csv', 'import_apprenants_csv');


function import_apprenants_csv() {
    echo '<p><a href="admin.php?page=simplon_add_menu">Retour au menu d\'administration</a></p>';

    $file = fopen($_FILES['csv']['tmp_name'], 'r');
    if($file){
        $fields = fgetcsv($file);
        
        while (($line = fgetcsv($file)) !== FALSE) {
            
          $apprenant = [];
          foreach($line as $key => $value) {
              $apprenant[$fields[$key]] = $value;
          }
          $apprenant['promo'] = $_POST['promo'];
          persist_apprenant($apprenant);

        }
        fclose($file);


    
        
    }
}


function persist_apprenant(array $data) {
    $username = strtolower(substr($data['Prénom'],0,1) . str_replace(' ','',$data['Nom']));
    $pwd = bin2hex(openssl_random_pseudo_bytes(8));
    $pwd = 'simplonapprenant';
    $id = wp_insert_user([
        'user_email' => $data['Mail'],
        'first_name' => $data['Prénom'],
        'last_name' => $data['Nom'],
        'user_login' => $username,
        'user_pass' => $pwd,
        'role' => 'apprenant'
    ]);


    if(is_wp_error($id)) {
        echo '<p>Erreur pour '.$data['Prénom'] . ' ' . $data['Nom'] . ' : <br>';
        echo $id->get_error_message();
        echo '</p>';
        return;
    } 

/*
    $content = "<p>Voici tes identifiants pour le site Simplon Lyon : <br>
        Login : ".$username."<br>
        Mot de Passe : ".$pwd."<br>

        Tu peux maintenant te connecter à l'adresse ".admin_url()." et gérer ton portfolio dans l'onglet Pages.        
        </p>";

    wp_mail('jdemel@simplon.co', 'Simplonlyon.fr - Inscription sur le site de portfolio', $content);
*/
    if(!update_field('promo', intval($data['promo']), 'user_'.$id)) {
        echo '<p>Erreur pour '.$data['Prénom'] . ' ' . $data['Nom'] . ' : Promo non assignée.</p>';
    }

    if(!empty($data['Entreprise'])) {
        update_field('entreprise', $data['Entreprise'], 'user_'.$id);
    }
    
    if(!empty($data['Type de Contrat'])) {
        update_field('type_de_contrat', $data['Type de Contrat'], 'user_'.$id);
    }

    echo '<p>Ajout de '.$data['Prénom'] . ' ' . $data['Nom'] .' réussi.</p>';

}



add_action('admin_init', 'register_options');

function register_options() {
    register_setting('simplon', 'active_promo');
}


add_filter('manage_promo_posts_columns', 'promo_table_head');

function promo_table_head( $defaults ) {
    $defaults['active_promo']  = 'Mise en Avant';
    $defaults['cursus']    = 'Cursus';
    return $defaults;
}

add_action( 'manage_promo_posts_custom_column', 'promo_table_content', 10, 2 );

function promo_table_content( $column_name, $post_id ) {
    if ($column_name == 'active_promo') {
        if(get_option('active_promo') == $post_id) {
            echo 'Mise en avant';
        } else {
            echo '<a href="admin-post.php?action=activate_promo&promo='.$post_id.'">Mettre en avant</a>';
        }
        
    }
    if ($column_name == 'cursus') {
        echo get_field('cursus', $post_id);
    }

}

add_action('admin_post_activate_promo', 'activate_promo');


function activate_promo() {
    update_option('active_promo', intval($_GET['promo']));
    wp_redirect(admin_url() . '/edit.php?post_type=promo');
}

