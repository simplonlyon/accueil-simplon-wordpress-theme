<!DOCTYPE html>
<html lang="fr">

<head <?php language_attributes(); ?>>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="title" content="Simplon Lyon" />
    <meta name="description" content="Simplon Lyon : une fabrique de developpeur.euse social et solidaire. Venez recruter votre propre dev motivé.e !" />
    <meta name="author" content="Clémence Laffont" />
<!--    <link rel="shortcut icon" href="img/avatar.png"> -->
    <script src="https://use.fontawesome.com/0eccec29f2.js"></script>
<!--    <meta property="og:image" content="https://www.simplonlyon.fr/AccueilSimplon/img/avatar.png" />
    <meta property="og:image:width" content="250" />
    <meta property="og:image:height" content="250" />-->
    <meta property="og:title" content="Simplon Lyon" />
    <meta property="og:description" content="Simplon Lyon : une fabrique de developpeur.euse social et solidaire. Venez recruter votre propre dev motivé.e !" />
	<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Nunito&display=swap" rel="stylesheet">
<!--    <link rel="image_src" href="https://www.simplonlyon.fr/img/avatar.png" /> -->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>

    <header class="nav">
        <div>
            <h1><a href="<?php echo get_home_url() ?>" title="Page d'accueil"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/simplon-ara.png" alt="logo de Simplon Lyon" /></a></h1>
            <svg id="deroulant" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 100 125" x="0px" y="0px">
                <title>122all</title>
                <rect x="16.56" y="45" width="66.88" height="10" />
                <rect x="16.56" y="21.92" width="66.88" height="10" />
                <rect x="16.56" y="68.08" width="66.88" height="10" />
            </svg>
        </div>
        <nav>
            <ul>
                <li>
                    <a href="<?php echo get_home_url() ?>">Accueil</a>
                </li>
                <li>
                    <a href="<?php the_permalink(get_option('active_promo')) ?>">Portfolios</a>
                    <ul class="sub_menu">

                        <?php $loop = new WP_Query(array('post_type' => 'promo', 'posts_per_page' => -1, 'orderBy' => 'date', 'order' => 'ASC')); ?>
                        <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                            <li><a href="<?php the_permalink() ?>"><?php the_title() ?></li></a>
                        <?php endwhile;
                        wp_reset_query(); ?>

                    </ul>
                </li>
            </ul>
        </nav>

    </header>
