<?php

get_header();

$page_id = get_queried_object_id();

?>

<main>
    <section class="en-tete">
        <h2>Portfolios</h2>
<?php $contacts =  get_field('contact', $page_id);
	if($contacts && count($contacts) > 0){
 ?>

        <p>Vous souhaitez rencontrer plusieurs candidat·e·s, contactez <?php foreach( $contacts as $index => $contact) { ?> <a href="mailto:<?php echo $contact->user_email ?>"><?php echo $contact->display_name ?></a> <?php echo ($index+1) == count($contacts)?'':' ou '; } ?></p>
<?php } ?>
    </section>

    <nav id="nav-promo">

        <?php $loop = new WP_Query(array('post_type' => 'promo', 'posts_per_page' => -1, 'orderBy' => 'date', 'order' => 'ASC')); ?>
        <?php while ($loop->have_posts()) : $loop->the_post(); ?>
            <a href="<?php the_permalink() ?>" <?php if (get_the_ID() == $page_id) {
                                                    echo 'class="promo-active"';
                                                } ?>><?php the_title() ?></a>

        <?php endwhile;
        wp_reset_query(); ?>


    </nav>

    <ul id="liste-apprenant">
        <?php

        $apprenants = get_users([
            'role' => 'apprenant',
            'meta_key' => 'promo',
            'meta_value' => $page_id
        ]);

        foreach ($apprenants as $apprenant) {
        ?>
            <li>
                <a href="<?php echo $apprenant->user_url; ?>">
                    <?php
                    $entreprise = get_field('entreprise', 'user_'. $apprenant->ID);
                    $typeContrat = get_field('type_de_contrat', 'user_'. $apprenant->ID);
                    if (!empty($entreprise)) {
                    ?>
                        <div class="emboche">
                            <h4>EN RUPTURE DE STOCK</h4>
                            <p>en <?php echo empty($typeContrat)?'poste':$typeContrat ?> chez <?php echo $entreprise ?></p>
                        </div>
                    <?php } ?>
                    
                        <h4>
                            <?php echo $apprenant->display_name ?>
                        </h4>
			<?php 
			$technos = get_field('technos', 'user_'.$apprenant->ID);
			if(!empty($technos) && empty($entreprise)) { ?>
                        <p class="technos">
                            <?php echo $technos; ?>
                        </p>
			<?php } ?>
                    
                    <?php echo get_avatar($apprenant->ID, 200) ?>
                </a>
            </li>
        <?php } ?>
    </ul>
</main>



<?php

get_footer();
